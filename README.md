# Read this file to ensure that the project is deployed correctly

## Slack API Integration

For this project to be fully functional, a slack app must be created with an incoming webhook to a channel with permission to access the slack API.
Store the slack API URL as a secure string parameter in AWS parameters with the name slack-app-access-key. This should be the only thing you need to create in the console - the rest should be deployed by the template.
See the [Slack API documentation](https://api.slack.com/quickstart) on how to create a Slack app that can send messages using webhooks.

## Order of Deployment

    1. s3buckets.yaml
    2. OACs.yaml
    3. cloudfrontdistributions.yaml
    4. cloudfrontbucketpolicies.yaml
    5. roles.yaml
    6. lambdas.yaml
    7. loggroups.yaml
    8. stepfunction.yaml
    9. eventrules.yaml

## Perfomance Notes

### How to run the application end-to-end

Create separate folders in the meetingaudiorecordings S3 bucket for each type of meeting (e.g. stand-ups, all-hands). Upload an mp3 or mp4 file to one of these folders. The application will not work if the files are not uploaded within a folder.

### Using Bedrock for Summarisation

- When deploying for production, change the number of tokens for the summarisation prompt and action items prompt to 8000 (which is the maximum number for Amazon Titan models). When testing other functionalities, consider keeping the number of tokens at a lower number to reduce costs. To understand more about model inference parameters read the documentation on [Model inference parameters](https://docs.aws.amazon.com/bedrock/latest/userguide/model-parameters.html). To understand more about the parameters in the textgenerationconfig (e.g. temperature and topP) read the documentation on [Inference parameters](https://docs.aws.amazon.com/bedrock/latest/userguide/inference-parameters.html?icmpid=docs_bedrock_help_panel_playgrounds)

- Can play around with using other bedrock AI models other such as Jurassic-2, Meta Llama, Claude, Mistral, Stability.ai Diffusion, Cohere that are more targeted towards summarisation. However it seems that Titan is recommended for summarisation and action items.

### Translation

- If translations in more languages are desired, add the languages to the appropriate lambda functions.

### Access to Original Recording

- The original recording does not get sent in the slack notification with this implementation because cloudfront links cannot be generated for mp4 files (the code for doing this is left commented in the template yaml files)

### Monitoring and Logging

- To monitor the execution of the project, go into AWS step functions and monitor the execution of the state machine that has been created. If a step has failed, you can open the CloudWatch log for it in the CloudWatch console.
- Logging can be viewed in the CloudWatch console. If there are errors it is likely to do with access but if the errors persist, consider adding print statements in the lambda functions.
